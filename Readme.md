# Retropie Menu

This is a simple menu project. It is designed to be the initial menu on my retropie, since I use the same box for both the Retropie and Steam streaming.

## Getting started

```bash
# Pull the code
git clone https://gitlab.com/terrabitz/retropie_menu
# cd into the directory
cd retropie_menu
# Install dependencies
pip3 install -r requirements.txt
# Run the menu
python3 menu.py
```

## Screenshot

![screenshot](media/screenshot.png)

## TODO

- [ ] Make the menu more easily configurable
