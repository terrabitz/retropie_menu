#!/usr/bin/python3
import os
import time
import subprocess
from pyfiglet import Figlet
from termcolor import cprint, colored

games = [("Steam",["/usr/bin/moonlight", "stream", "-app", "Steam",
                   "192.168.1.11"]),
         ("RetroPie",["/usr/bin/emulationstation"])]

def main():
    os.system('clear')
    selected = 0
    f = Figlet(font='big')
    while True:
        for index,item in enumerate(games):
            index_color = 'red'
            text_color = 'white'
            rendered_index = f.renderText('{}'.format(index+1))
            rendered_text = f.renderText('{}'.format(item[0]))
            cprint(rendered_index, index_color, end='')
            cprint(rendered_text, text_color, end='')
            print()
            print()
        selection = input('Make a selection [{}-{}]:  '.format(
            1,
            len(games))
        )
        try:
            selection_int = int(selection)
        except:
            continue
        selection_int = selection_int - 1
        if 0 <= selection_int < len(games):
            subprocess.call(games[selection_int][1])
        else:
            cprint(
                'Not a valid selection, please choose a number between {} and'+
                    '{}'.format(1, len(games)),
                'red'
            )
        time.sleep(2)
        os.system('clear')

if __name__=='__main__':
    main()

